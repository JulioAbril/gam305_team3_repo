// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EnemyAi.generated.h"

UCLASS()
class GAM305_TEAM3_PROJECT_API AEnemyAi : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemyAi();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// take damage function
	UFUNCTION(BlueprintCallable)
	void TakeDamage(float _damage);
	// health variable of enemy character
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Enemy)
	float health;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
