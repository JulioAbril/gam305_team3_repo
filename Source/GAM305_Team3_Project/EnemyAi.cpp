// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAi.h"

// Sets default values
AEnemyAi::AEnemyAi()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// default health value. 1.0 = 100%
	health = 1.0f;
}

// Called when the game starts or when spawned
void AEnemyAi::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEnemyAi::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemyAi::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

// take damage function, sets health equal to health - damage
void AEnemyAi::TakeDamage(float _damage)
{
	health -= _damage;
}