// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAIActor.h"

// Sets default values
AEnemyAIActor::AEnemyAIActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// set default health. 1.0f = 100%
	health = 1.0f;
}

// Called when the game starts or when spawned
void AEnemyAIActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEnemyAIActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//take damage function sets health value to health - damage
void AEnemyAIActor::TakeDamage(float _damage)
{
	health -= _damage;
}